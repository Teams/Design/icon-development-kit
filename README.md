# Icon Development Kit

![Icon Guidelines](doc/icon-development-kit-guidelines.png)

This project provides symbolic icon assets to be used by application developers. It is liberally licensed (CC0) not to excluding any kind of application, including proprietary.

Please refer to the [GNOME Human Interface Guidelines](https://developer.gnome.org/hig/), particularly the [user interface icons section](https://developer.gnome.org/hig/guidelines/ui-icons.html), for general guidelines on the style and use.

## Contribution Guide

If you'd like to contribute to the set, an issue/request must be [filed in gitlab](https://gitlab.gnome.org/Teams/Design/icon-development-kit/-/issues) first. Subsequently a merge request can be filed that implements the request.

#### Technical Constraints

Symbolic icons in the development kit are strictly monochrome. The only way to introduce subtle shading is with opacity. It is, however, strictly recommended to avoid shading and rely on a simple silhouette to form the icon. If you can't avoid shading with opacity, only use `35%` and `50%` values. 

There are currently some constraints that you have to obey to avoid misrenderings in apps:

  - Don't set global object opacity value in Inkscape, but set the `alpha` value of a color instead to get proper rendering in Symbolic Preview and all `librsvg` based renderers.
  - Don't use stroke objects. You can 'bake' outlines into shapes with *Path>Stroke to Path* in Inkscape or use a shape effect called *Join Type* that allows you to keep the basic shape of a path, but get an automatic export to shapes when saving.

### Dimensions 

Individual icons are 16x16px nominally. Each icon is enclosed in a rectangle of 16x16px for easy grid positioning and allows the tools to export them. This rectangle has no fill and no outline set. It is removed during export.

An icon is defined as a group. The group contains some metadata that define keywords it can be found under and name for the file. To provide this metadata, use Inkscape's `object property` dialog. Filename is defined as `title` (`<svg:title></svg:title>`) while the keywords are space separated `label`.

Keep the icon tiles on the grid using snap to grid. 

### Style Considerations

Since GNOME 40, the style of the symbolics shifted away from the filled shapes to outlines. Most common case is to use 2px nominal width for strokes. There are case where more intricate detail is necessary where 2px lines are too thick. In rare cases an outline variant isn't possible in the contrained 16x16px nominal canvas and the old style filled shape is necessary.

### Naming

Arranging the icons in layers/contexts is not mandatory, but helps when browsing for the icon in [*Symbolic Preview*](https://flathub.org/apps/org.gnome.design.SymbolicPreview) as opposed to searching. The best way to name an icon is to use a descriptive name rather than trying to identify its purpose (ie *handshake* rather than *introduction*). Semantic naming has proven to introduce more problems than it solves.

To avoid certain assets to be exported with Symbolic preview, put them in a layer that starts with `noexport-`.

#### Search

To be able to find the icon, search **keywords** need to be provided. This is done similarly to defining a title, in the object properties panel in Inkscape. For the icon group, provide a space separated list of all keywords you think the icon should be found under.

![Icon Metadata](doc/icon-metadata.png)

### Symbolic Preview

The best workflow creating symbolic icons is to use the [Symbolic Preview](https://flathub.org/apps/details/org.gnome.design.SymbolicPreview) application. This app is useful not only to contribute to `icon-development-kit`, but to manage your own library of symbols, in cases where you use more than a handful of custom icons in your app.

![Symbolic Preview](doc/symbolic-preview-screenshot.png)

For more info on the Symbolic Preview workflow, I've written a [short blog post](https://blog.jimmac.eu/2021/how-to-symbolic/).
